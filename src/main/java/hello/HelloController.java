package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        String style = "<style type='text/css' media='screen'>";
        style += "body { background-color: #6DB23E; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }";
        style += "</style>";
        style += "<head><script defer";
        style += "data-project-id='7764'";
        style += "data-merge-request-id='24072'";
        style += "data-mr-url='https://gitlab.com'";
        style += "id='review-app-toolbar-script'";
        style += "src='https://gitlab.com/assets/webpack/visual_review_toolbar.js'";
        style += "</head>"; 
/> 
        
        // TODO - Personalize this message!
        String message = "Hello from Spring github!!!!!!";
        
        String body = "<body>" + message + "</body>";

        return style + body;
    }

}
